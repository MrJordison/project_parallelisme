#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <device_launch_parameters.h>
#include <cuda_runtime.h>
//#include "src\ppm_lib.h"
#include "src\ppm_lib.c"

using namespace std;

static void HandleError(cudaError_t err,
	const char *file,
	int line) {
		if (err != cudaSuccess) {
				printf("%s in %s at line %d\n", cudaGetErrorString(err), file, line);
				exit(EXIT_FAILURE);
		}
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

__device__ int* get_pixel(int * input, int &x, int &y, int& width, int& height) {
	int index = (width * y + x) * 3;
	int result[3];
	if (index >= 0 && index < (width * height * 3)) {
		result[0] = input[index];
		result[1] = input[index + 1];
		result[2] = input[index + 2];
	}
	else {
		result[0] = result[1] = result[2] = 0;
	}
	return result;
}

__global__ void filtreVersion6(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	//Coordonn�es 2D pixel dans input / output 
	unsigned int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3; //index 1D du premier canal du pixel dans input / output

												   //Variables primitives en shared memory

		__shared__ int sharedFilterN;
		__shared__ int sharedWidth;
		__shared__ int sharedHeight;
		__shared__ int sharedDivider;

		//variables simples dans sharedMemory (�crites par 1er thread du bloc)
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			sharedFilterN = filterN;
			sharedWidth = width;
			sharedHeight = height;
			sharedDivider = divider;
		}
		//Mise en attente des autres threads jusqu'� ce que le premier aie fini l'�criture des shared variables
		__syncthreads();

		//tableau de donn�es partag�es dans le bloc
		extern __shared__ int bigTable[];
		int * sharedFilter = bigTable; //Filtre partag�
		int * sharedInput = &sharedFilter[sharedFilterN * sharedFilterN];

		//copie du filtre
		int idBlock1D = threadIdx.x + threadIdx.y * blockDim.x; //index pixel 1D dans block

		//Ecriture du filtre en partag� par les 25 premiers threads du block
		if (idBlock1D < (sharedFilterN * sharedFilterN))
			sharedFilter[idBlock1D] = filter[idBlock1D];

		int halfDim = sharedFilterN / 2; // demie-dimension du noyau de convolution


		//Traitement de la copie de la zone d'input par ligne : un thread va copier une ligne
		if (idBlock1D < (blockDim.y + 2 * halfDim)) {
			
			int xBeginLine = (blockDim.x * blockIdx.x - halfDim);
			int yBeginLine = (blockDim.y * blockIdx.y - halfDim + idBlock1D);
			
			int beginLineSharedInput1D = idBlock1D * (blockDim.x + 2 * halfDim);

			int idLineSharedInput1D3C; //indice de ligne dans sharedInput
			int idLineInput1D3C; //indice 1D premier canal du pixel dans la ligne dans input
			int x_temp;
			for (int i = 0; i < blockDim.x + 2 * halfDim; ++i) {
				x_temp = xBeginLine + i;
				
				idLineSharedInput1D3C = (beginLineSharedInput1D + i) * 3;

				if (yBeginLine >= 0 && yBeginLine < sharedHeight && x_temp >= 0 && x_temp < sharedWidth) {
					idLineInput1D3C = (sharedWidth * yBeginLine + x_temp) * 3;

					sharedInput[idLineSharedInput1D3C] = input[idLineInput1D3C];
					sharedInput[idLineSharedInput1D3C + 1] = input[idLineInput1D3C + 1];
					sharedInput[idLineSharedInput1D3C + 2] = input[idLineInput1D3C + 2];
				}
				else {
					sharedInput[idLineSharedInput1D3C] =  
					sharedInput[idLineSharedInput1D3C + 1] = 
					sharedInput[idLineSharedInput1D3C + 2] = 0;
				}
				
				
				

			}
		}

		__syncthreads();
		if (x_dim < sharedWidth) {
			int gridCounter = 0; // reset some values //index 1D dans noyau convolution (filtre)

			int r, g, b;//Buffer des canaux o� l'on va additionner les valeurs des voisins
			r = g = b = 0;

			//coordonn�es pour calculer les coordonn�es des voisins
			int index_temp;

			int sharedInputWidth = blockDim.x + 2 * halfDim;

			//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
			for (int y2 = -halfDim; y2 <= halfDim; y2++)
				for (int x2 = -halfDim; x2 <= halfDim; x2++) {

					//Calcul des coordonn�es du pixel voisin courant (coordonn�es 2D pixel dans input / output)
					index_temp = (threadIdx.x + halfDim + x2 + (threadIdx.y + halfDim + y2) * sharedInputWidth) * 3;


					r += sharedInput[index_temp] * sharedFilter[gridCounter];
					g += sharedInput[index_temp + 1] * sharedFilter[gridCounter];
					b += sharedInput[index_temp + 2] * sharedFilter[gridCounter];
					
					// Go to the next value on the filter grid
					gridCounter++;

				}

			//Division du buffer par le facteur diviseur associ� au noyau de convolution
			r /= sharedDivider;
			g /= sharedDivider;
			b /= sharedDivider;


			output[index] = r;
			output[index + 1] = g;
			output[index + 2] = b;
			

		}
		
	}
}

__global__ void filtreVersion5(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	//Coordonn�es 2D pixel dans input / output 
	int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (x_dim < width && y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3; //index 1D du premier canal du pixel dans input / output

												   //Variables primitives en shared memory

		__shared__ int sharedFilterN;
		__shared__ int sharedWidth;
		__shared__ int sharedHeight;
		__shared__ int sharedDivider;

		//variables simples dans sharedMemory (�crites par 1er thread du bloc)
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			sharedFilterN = filterN;
			sharedWidth = width;
			sharedHeight = height;
			sharedDivider = divider;
		}
		//Mise en attente des autres threads jusqu'� ce que le premier aie fini l'�criture des shared variables
		__syncthreads();

		//tableau de donn�es partag�es dans le bloc
		extern __shared__ int bigTable[];
		int * sharedFilter = bigTable; //Filtre partag�
		int * sharedInput = &sharedFilter[sharedFilterN * sharedFilterN];

		//copie du filtre
		int idBlock1D = threadIdx.x + threadIdx.y * blockDim.x; //index pixel 1D dans block

																//Ecriture du filtre en partag� par les 25 premiers threads du block
		if (idBlock1D < (sharedFilterN * sharedFilterN))
			sharedFilter[idBlock1D] = filter[idBlock1D];


		int halfDim = sharedFilterN / 2; // demie-dimension du noyau de convolution

		//�criture du bloc d'input utilis� par le premier thread du bloc
		//copie de la zone d'input utilis�e
		int sIid = ((blockDim.x + halfDim * 2) * (threadIdx.y + halfDim) + threadIdx.x + halfDim) * 3; //emplacement du pixel g�r� par le thread dans le bloc
		sharedInput[sIid] = input[index];
		sharedInput[sIid + 1] = input[index + 1];
		sharedInput[sIid + 2] = input[index + 2];

		int * value;
		if (threadIdx.x == 0) { // si pixel bord gauche, r�cup padding � gauche

								//R�cup�re les pixels dans le padding de gauche
			for (int i = -halfDim; i < 0; ++i) {
				int xtemp = x_dim + i;
				value = get_pixel(input, xtemp, y_dim, sharedWidth, sharedHeight);
				sharedInput[sIid + i * 3] = value[0];
				sharedInput[sIid + i * 3 + 1] = value[1];
				sharedInput[sIid + i * 3 + 2] = value[2];

				if (threadIdx.y == 0) { // coin haut gauche
					for (int j = -halfDim; j < 0; ++j) {
						int ytemp = y_dim + j;
						value = get_pixel(input, xtemp, ytemp, sharedWidth, sharedHeight);
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3] = value[0];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
					}
				}

				else if (threadIdx.y == blockDim.y - 1) { // coin bas gauche
					for (int j = 1; j <= halfDim; ++j) {
						int ytemp = y_dim + j;
						value = get_pixel(input, xtemp, ytemp, sharedWidth, sharedHeight);
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3] = value[0];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
					}
				}
			}
		}
		else if (threadIdx.x == blockDim.x - 1) { // si pixel bord droit, r�cup padding � droit

												  //R�cup�re les pixels dans le padding de droite
			for (int i = 1; i <= halfDim; ++i) {
				int xtemp = x_dim + i;
				value = get_pixel(input, xtemp, y_dim, sharedWidth, sharedHeight);
				sharedInput[sIid + i * 3] = value[0];
				sharedInput[sIid + i * 3 + 1] = value[1];
				sharedInput[sIid + i * 3 + 2] = value[2];

				if (threadIdx.y == 0) { // coin haut droit
					for (int j = -halfDim; j < 0; ++j) {
						int ytemp = y_dim + j;
						value = get_pixel(input, xtemp, ytemp, sharedWidth, sharedHeight);
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3] = value[0];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
					}
				}

				else if (threadIdx.y == blockDim.y - 1) { // coin bas droit
					for (int j = 1; j <= halfDim; ++j) {
						int ytemp = y_dim + j;
						value = get_pixel(input, xtemp, ytemp, sharedWidth, sharedHeight);
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3] = value[0];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
						sharedInput[sIid + (i + (blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
					}
				}
			}
		}
		//Bord haut
		if (threadIdx.y == 0) {
			for (int j = -halfDim; j < 0; ++j) {
				int ytemp = y_dim + j;
				value = get_pixel(input, x_dim, ytemp, sharedWidth, sharedHeight);
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3] = value[0];
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
			}
		}
		//Bord haut
		if (threadIdx.y == blockDim.y - 1) {
			for (int j = 1; j <= halfDim; ++j) {
				int ytemp = y_dim + j;
				value = get_pixel(input, x_dim, ytemp, sharedWidth, sharedHeight);
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3] = value[0];
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3 + 1] = value[1];
				sharedInput[sIid + ((blockDim.x + 2 * halfDim) * j) * 3 + 2] = value[2];
			}
		}

		__syncthreads();


		int gridCounter = 0; // reset some values //index 1D dans noyau convolution (filtre)

		int r, g, b;//Buffer des canaux o� l'on va additionner les valeurs des voisins
		r = g = b = 0;

		//coordonn�es pour calculer les coordonn�es des voisins
		int index_temp;

		int sharedInputWidth = blockDim.x + 2 * halfDim;

		//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
		for (int y2 = -halfDim; y2 <= halfDim; y2++)
			for (int x2 = -halfDim; x2 <= halfDim; x2++) {

				//Calcul des coordonn�es du pixel voisin courant (coordonn�es 2D pixel dans input / output)
				index_temp = (threadIdx.x + halfDim + x2 + (threadIdx.y + halfDim + y2) * sharedInputWidth) * 3;


				r += sharedInput[index_temp] * sharedFilter[gridCounter];
				g += sharedInput[index_temp + 1] * sharedFilter[gridCounter];
				b += sharedInput[index_temp + 2] * sharedFilter[gridCounter];

				// Go to the next value on the filter grid
				gridCounter++;

			}

		__syncthreads();


		//Division du buffer par le facteur diviseur associ� au noyau de convolution
		r /= sharedDivider;
		g /= sharedDivider;
		b /= sharedDivider;


		output[index] = r;
		output[index + 1] = g;
		output[index + 2] = b;


	}
}

__global__ void filtreVersion4(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	//Coordonn�es 2D pixel dans input / output 
	int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (x_dim < width && y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3; //index 1D du premier canal du pixel dans input / output

												   //Variables primitives en shared memory

		__shared__ int sharedFilterN;
		__shared__ int sharedWidth;
		__shared__ int sharedHeight;
		__shared__ int sharedDivider;

		//variables simples dans sharedMemory (�crites par 1er thread du bloc)
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			sharedFilterN = filterN;
			sharedWidth = width;
			sharedHeight = height;
			sharedDivider = divider;
		}
		//Mise en attente des autres threads jusqu'� ce que le premier aie fini l'�criture des shared variables
		__syncthreads();

		//tableau de donn�es partag�es dans le bloc
		extern __shared__ int bigTable[];
		int * sharedFilter = bigTable; //Filtre partag�
		int * sharedInput = &sharedFilter[sharedFilterN * sharedFilterN];

		//copie du filtre
		int idBlock1D = threadIdx.x + threadIdx.y * blockDim.x; //index pixel 1D dans block

		//Ecriture du filtre en partag� par les 25 premiers threads du block
		if (idBlock1D < (sharedFilterN * sharedFilterN))
			sharedFilter[idBlock1D] = filter[idBlock1D];

		__syncthreads();

		
		int halfDim = sharedFilterN / 2; // demie-dimension du noyau de convolution

		//�criture du bloc d'input utilis� par le premier thread du bloc
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			
			int idBeginSharedInput1D = 0;

			int idInput1D_temp; //index 1D du pixel dans input � copier

			
			int i, j;
			for (j = y_dim - halfDim; j < (int)(y_dim - halfDim + (blockDim.y + 2 * halfDim)); ++j) {
				
				for (i = x_dim - halfDim; i < (int)(x_dim - halfDim + (blockDim.x + 2 * halfDim)); ++i) {
					
					if (i >= 0 && j >= 0 && i < sharedWidth && j < sharedHeight) {
						idInput1D_temp = (j * sharedWidth + i) * 3;
						sharedInput[idBeginSharedInput1D] = input[idInput1D_temp];
						sharedInput[idBeginSharedInput1D + 1] = input[idInput1D_temp + 1];
						sharedInput[idBeginSharedInput1D + 2] = input[idInput1D_temp + 2];	
					}
					else {
						sharedInput[idBeginSharedInput1D] = sharedInput[idBeginSharedInput1D + 1] = sharedInput[idBeginSharedInput1D + 2] = 0;
					}

					idBeginSharedInput1D += 3;
				}
				
			}
		}

		__syncthreads();

		int gridCounter = 0; // reset some values //index 1D dans noyau convolution (filtre)

		int r, g, b;//Buffer des canaux o� l'on va additionner les valeurs des voisins
		r = g = b = 0;

		//coordonn�es pour calculer les coordonn�es des voisins
		int index_temp;

		int sharedInputWidth = blockDim.x + 2 * halfDim;

		//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
		for (int y2 = -halfDim; y2 <= halfDim; y2++)
			for (int x2 = -halfDim; x2 <= halfDim; x2++) {

				//Calcul des coordonn�es du pixel voisin courant (coordonn�es 2D pixel dans input / output)
				index_temp = (threadIdx.x + halfDim + x2 + (threadIdx.y + halfDim + y2) * sharedInputWidth) * 3;


				r += sharedInput[index_temp] * sharedFilter[gridCounter];
				g += sharedInput[index_temp + 1] * sharedFilter[gridCounter];
				b += sharedInput[index_temp + 2] * sharedFilter[gridCounter];

				// Go to the next value on the filter grid
				gridCounter++;

				}

		__syncthreads();
	
		
		//Division du buffer par le facteur diviseur associ� au noyau de convolution
		r /= sharedDivider;
		g /= sharedDivider;
		b /= sharedDivider;
		

		output[index] = r;
		output[index + 1] = g;
		output[index + 2] = b;

	}
}

__global__ void filtreVersion3(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	//Coordonn�es 2D pixel dans input / output 
	unsigned int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (x_dim < width && y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3; //index 1D du premier canal du pixel dans input / output

												   //Variables primitives en shared memory

		__shared__ int sharedFilterN;
		__shared__ int sharedWidth;
		__shared__ int sharedHeight;
		__shared__ int sharedDivider;

		//tableau de donn�es partag�es dans le bloc
		extern __shared__ int bigTable[];
		int * sharedFilter = bigTable; //Filtre partag�
		//int * sharedInput = &sharedFilter[sharedFilterN * sharedFilterN];

		//variables simples dans sharedMemory (�crites par 1er thread du bloc)
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			sharedFilterN = filterN;
			sharedWidth = width;
			sharedHeight = height;
			sharedDivider = divider;

			for (int i = 0; i < sharedFilterN * sharedFilterN; ++i)
				sharedFilter[i] = filter[i];
		}
		//Mise en attente des autres threads jusqu'� ce que le premier aie fini l'�criture des shared variables
		__syncthreads();

		int halfDim = sharedFilterN / 2; // demie-dimension du noyau de convolution


		int gridCounter = 0; // reset some values //index 1D dans noyau convolution (filtre)

		int r, g, b;//Buffer des canaux o� l'on va additionner les valeurs des voisins
		r = g = b = 0;

		//coordonn�es pour calculer les coordonn�es des voisins
		int x_temp, y_temp, index_temp;

		//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
		for (int y2 = -halfDim; y2 <= halfDim; y2++)
			for (int x2 = -halfDim; x2 <= halfDim; x2++) {

				//Calcul des coordonn�es du pixel voisin courant (coordonn�es 2D pixel dans input / output)
				x_temp = x_dim + x2;
				y_temp = y_dim + y2;

				//Si les coordonn�es du voisin courant sont dans l'image, on ajoute sa valeur au buffer
				if (x_temp >= 0 && y_temp >= 0 && x_temp < sharedWidth && y_temp < sharedHeight) {

					index_temp = 3 * y_temp * sharedWidth + x_temp * 3; // coordonn�e 1D du premier canal du pixel voisin courant dans input

					r += input[index_temp] * sharedFilter[gridCounter];
					g += input[index_temp + 1] * sharedFilter[gridCounter];
					b += input[index_temp + 2] * sharedFilter[gridCounter];

					// Go to the next value on the filter grid
					gridCounter++;

				}
			}

		//Division du buffer par le facteur diviseur associ� au noyau de convolution
		r /= sharedDivider;
		g /= sharedDivider;
		b /= sharedDivider;


		output[index] = r;
		output[index + 1] = g;
		output[index + 2] = b;


	}
}

__global__ void filtreVersion2(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	//Coordonn�es 2D pixel dans input / output 
	unsigned int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (x_dim < width && y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3; //index 1D du premier canal du pixel dans input / output

		//Variables primitives en shared memory

		__shared__ int sharedFilterN;
		__shared__ int sharedWidth;
		__shared__ int sharedHeight;
		__shared__ int sharedDivider;

		//variables simples dans sharedMemory (�crites par 1er thread du bloc)
		if (threadIdx.x == 0 && threadIdx.y == 0) {
			sharedFilterN = filterN;
			sharedWidth = width;
			sharedHeight = height;
			sharedDivider = divider;
		}
		//Mise en attente des autres threads jusqu'� ce que le premier aie fini l'�criture des shared variables
		__syncthreads();

		//tableau de donn�es partag�es dans le bloc
		extern __shared__ int bigTable[];
		int * sharedFilter = bigTable; //Filtre partag�

		//copie du filtre
		int idBlock1D = threadIdx.x + threadIdx.y * blockDim.x; //index pixel 1D dans block
		
		//Ecriture du filtre en partag� par les 25 premiers threads du block
		if (idBlock1D < (sharedFilterN * sharedFilterN))
			sharedFilter[idBlock1D] = filter[idBlock1D];

		__syncthreads();

		int halfDim = sharedFilterN / 2; // demie-dimension du noyau de convolution


		int gridCounter = 0; // reset some values //index 1D dans noyau convolution (filtre)
 
		int r, g, b;//Buffer des canaux o� l'on va additionner les valeurs des voisins
		r = g = b = 0;

		//coordonn�es pour calculer les coordonn�es des voisins
		int x_temp, y_temp, index_temp;

		//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
		for (int y2 = -halfDim; y2 <= halfDim; y2++)
			for (int x2 = -halfDim; x2 <= halfDim; x2++) {

				//Calcul des coordonn�es du pixel voisin courant (coordonn�es 2D pixel dans input / output)
				x_temp = x_dim + x2;
				y_temp = y_dim + y2;

				//Si les coordonn�es du voisin courant sont dans l'image, on ajoute sa valeur au buffer
				if (x_temp >= 0 && y_temp >= 0 && x_temp < sharedWidth && y_temp < sharedHeight) {

					index_temp = 3 * y_temp * sharedWidth + x_temp * 3; // coordonn�e 1D du premier canal du pixel voisin courant dans input

					r += input[index_temp] * sharedFilter[gridCounter];
					g += input[index_temp + 1] * sharedFilter[gridCounter];
					b += input[index_temp + 2] * sharedFilter[gridCounter];

					// Go to the next value on the filter grid
					gridCounter++;

				}
			}

		//Division du buffer par le facteur diviseur associ� au noyau de convolution
		r /= sharedDivider;
		g /= sharedDivider;
		b /= sharedDivider;


		output[index] = r;
		output[index + 1] = g;
		output[index + 2] = b;


	}
}

//Kernel ou un thread g�re les 3 canaux RGB d'un pixel
__global__ void filtreVersion1(int* input, int *output, int *filter, int filterN, int width, int height, int divider) {

	unsigned int x_dim = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int y_dim = blockIdx.y * blockDim.y + threadIdx.y;

	//Si les coordonn�es du pixel associ� au thread sont hors de l'image, il ne fait rien
	if (x_dim < width && y_dim < height) {

		int index = 3 * width * y_dim + x_dim * 3;

		int halfDim = filterN / 2; // demie-dimension du noyau de convolution
		

		int gridCounter = 0; // reset some values

		//Buffer des canaux o� l'on va additionner les valeurs des voisins
		int r, g, b;
		r = g = b = 0;

		//coordonn�es pour calculer les coordonn�es des voisins
		int x_temp, y_temp, index_temp;
				
		//It�ration sur les voisins du pixel courant d'apr�s le noyau de convolution
		for (int y2 = -halfDim; y2 <= halfDim; y2++)
			for (int x2 = -halfDim; x2 <= halfDim; x2++) { 
				//Calcul des coordonn�es du pixel voisin courant
				x_temp = x_dim + x2;
				y_temp = y_dim + y2;

				//Si les coordonn�es du voisin courant sont dans l'image, on ajoute sa valeur au buffer
				if (x_temp >= 0 && y_temp >= 0 && x_temp < width && y_temp < height) {
					
					index_temp = 3 * y_temp * width + x_temp * 3; // index du premier canal du voisin courant dans l'image d'entr�e
					
					r += input[index_temp] * filter[gridCounter];
					g += input[index_temp + 1] * filter[gridCounter];
					b += input[index_temp + 2] * filter[gridCounter];

					// Go to the next value on the filter grid
					gridCounter++;

				}				
			}
		
		//Division du buffer par le facteur diviseur associ� au noyau de convolution
		r /= divider;
		g /= divider;
		b /= divider;


		output[index] = r;
		output[index + 1] = g;
		output[index + 2] = b;


	}
}

int* runCUDA(int typeKernel, double& time, int* image, int width, int height, int* filter, int filterN, int divider, int blocDim) {

	size_t buffer_size = sizeof(int) * width * height * 3;

	int *image_out = new int[buffer_size];

	int *gpu_output;
	int *gpu_input;

	int *gpu_filter;

	HANDLE_ERROR(cudaMalloc((void **)&gpu_input, buffer_size));
	HANDLE_ERROR(cudaMalloc((void **)&gpu_output, buffer_size));
	HANDLE_ERROR(cudaMalloc((void **)&gpu_filter, sizeof(int) * filterN * filterN));

	HANDLE_ERROR(cudaMemcpy(gpu_input, image, buffer_size, cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(gpu_filter, filter, sizeof(int) * filterN * filterN, cudaMemcpyHostToDevice));


	dim3 blockDim(blocDim, blocDim);

	float wGrid = (float)width / (float)blockDim.x;
	float hGrid = (float)height / (float)blockDim.y;

	if (wGrid > (int)wGrid)
		wGrid++;
	if (hGrid > (int)hGrid)
		hGrid++;

	dim3 gridDim((int)wGrid, (int)hGrid);

	/*Cr�ation et lancement du timer du kernel */
	cudaEvent_t start, stop;
	float iteration_time;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start, 0);
	
	if (typeKernel == 0){
		/*### Lancement kernel simple ###*/
		filtreVersion1 <<<gridDim, blockDim>>>(gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}

	else if (typeKernel == 1) {
		/*### Lancement kernel shared variables simples + filtre par plusieurs threads ###*/
			size_t allocatedSharedArray = (filterN * filterN) * sizeof(int);
		filtreVersion2 << <gridDim, blockDim, allocatedSharedArray >> > (gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}
	else if (typeKernel == 2) {
		/*### Lancement kernel shared variables simples + filtre par un seul thread ###*/
		size_t allocatedSharedArray = (filterN * filterN) * sizeof(int);
		filtreVersion3<< <gridDim, blockDim, allocatedSharedArray >> > (gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}
	else if (typeKernel == 3) {
		/*### Lancement kernel full shared (m�me que shared1) et input �crit en partag� par un seul thread ###*/
		int halfDim = filterN / 2;
		size_t allocatedSharedArray = (filterN * filterN + ((blockDim.x + 2 * halfDim) * (blockDim.y + 2 * halfDim)) * 3) * sizeof(int);
		filtreVersion4<<<gridDim, blockDim, allocatedSharedArray>>>(gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}
	else if (typeKernel == 4) {
		/*### Lancement kernel full shared (m�me que shared1) et input �crit en partag� par tous les threads ###*/
		int halfDim = filterN / 2;
		size_t allocatedSharedArray = (filterN * filterN + ((blockDim.x + 2 * halfDim) * (blockDim.y + 2 * halfDim)) * 3) * sizeof(int);
		filtreVersion5<<<gridDim, blockDim, allocatedSharedArray>>>(gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}
	else if (typeKernel == 5) {
		/*### Lancement kernel full shared (m�me que shared1) et input �crit en partag� avec un thread par ligne ###*/
		int halfDim = filterN / 2;
		size_t allocatedSharedArray = (filterN * filterN + ((blockDim.x + 2 * halfDim) * (blockDim.y + 2 * halfDim)) * 3) * sizeof(int);
		filtreVersion6 << <gridDim, blockDim, allocatedSharedArray >> >(gpu_input, gpu_output, gpu_filter, filterN, width, height, divider);
	}
	
	

	//Barri�re d'attente de la fin de tous les threads avant de continuer le s�quentielle
	cudaThreadSynchronize();

	//Fin du timer et calcul du temps d'exe du kernel
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&iteration_time, start, stop);
	time = (double)iteration_time;
	//printf(">%s\n", cudaGetErrorString(cudaGetLastError()));

	//printf("Temps d execution kernel : %.3f ms \n", time);

	cudaMemcpy(image_out, gpu_output, buffer_size, cudaMemcpyDeviceToHost);

	cudaFree(gpu_input);
	cudaFree(gpu_output);
	cudaFree(gpu_filter);

	return image_out;
}

int* convertFromImage(PPMImage *image, int &dim) {

	dim = image->x*image->y;
	int* input = new int[dim * 3];

	int tmpIndex;
	for (int i = 0; i < image->x * image->y; i++) {
		tmpIndex = i * 3;
		input[tmpIndex] = image->data[i].red;
		input[tmpIndex+1] = image->data[i].green;
		input[tmpIndex+2] = image->data[i].blue;
	}

	return input;

}

void convertToImage(PPMImage *origin, int* input, int dim) {

	int tmpIndex;
	for (int i = 0; i < dim; i++) {
		tmpIndex = i * 3;
		origin->data[i].red = input[tmpIndex];
		origin->data[i].green = input[tmpIndex + 1];
		origin->data[i].blue = input[tmpIndex + 2];
	}

}

int main(void) {

        //Param�tres � changer pour l'ex�cution des kernels
        int kernel_version = 0;
        int blocDim = 16;
        int nb_iterations = 100;
	int filter[] = { 0,0,0,0,0,0,1,3,1,0,0,3,5,3,0,0,1,3,1,0,0,0,0,0,0 };
	int filterN = 5;
	int divider = 21;

	PPMImage *image = readPPM("image.ppm");

	int dim = 0;
	int *img = convertFromImage(image, dim);
	int *img_output;

	cout << "Lancement du kernel version " << kernel_version << " sur " << nb_iterations << " iterations " << endl;
	cout << "Taille de bloc : " << blocDim << " * " << blocDim << endl;

	double total_time = 0.;
	double time = 0;
	for (int i = 1; i <= nb_iterations; ++i) {
		cout << "\rAvancement : " << i << " sur " << nb_iterations;

		img_output = runCUDA(kernel_version, time, img, image->x, image->y, filter, filterN, divider, blocDim);
		free(img);
		img = img_output;
		total_time += time;
	}
	cout << "\nTemps total d'execution des passes kernel : " << total_time << " ms" << endl;

	total_time /= (double)nb_iterations;
	cout << "Temps moyen d'execution d'une passe du kernel : " << total_time << " ms" << endl;

	convertToImage(image, img_output, dim);
	writePPM("output2.ppm", image);

	free(img_output);
	free(image);

	system("pause");
	return 0;

}
