#include <stdio.h>
#include <omp.h>
#include <stdlib.h>
#include <cmath>
#include <time.h>
//#include "src\ppm_lib.h"
#include "src\ppm_lib.c"

#define OCCUPATION 1

using namespace std;

//Fonction applicant un filtre sur le pixel concerne
int* applyFilter(PPMImage *image, int filter[], int filterN, int divisionFactor, int position) {

	//On recupere la position du pixel
	int y = position / image->x;
	int x = position % image->x;

	int diff = filterN / 2;
	int xBegin = x - diff;
	int yBegin = y - diff;

	//On initialise la liste qui contiendra les composantes r,g,b de la couleur
	int *colorF = new int[3];
	colorF[0] = 0;
	colorF[1] = 0;
	colorF[2] = 0;

	//Pour chaque pixel concerne par le filtre
	for (int x2 = xBegin; x2 <= x + diff; x2++) {

		if (x2 >= 0 && x2 < image->x) {

			for (int y2 = yBegin; y2 <= y + diff; y2++) {

				if (y2 >= 0 && y2 < image->y) {

					//On recupere la valeur du filtre correspondant au pixel
					int filterPos = x2 - xBegin + (y2 - yBegin)*filterN;

					int npos = y2 * image->x + x2;

					//On ajoute cette valeur a la couleur finale
					colorF[0] += image->data[npos].red * filter[filterPos];
					colorF[1] += image->data[npos].green * filter[filterPos];
					colorF[2] += image->data[npos].blue * filter[filterPos];

				}

			}
		}

	}

	//On divise la couleur par le facteur du filtre
	colorF[0] /= divisionFactor;
	colorF[1] /= divisionFactor;
	colorF[2] /= divisionFactor;

	return colorF;

}

int main() {

	PPMImage *image = readPPM("image.ppm");

	//Filtres
	int filter[] = { 0,0,0,0,0,0,1,3,1,0,0,3,5,3,0,0,1,3,1,0,0,0,0,0,0 };
	int filterSobelHorizontal[] = {1, 2, 0, -2, -1, 4, 8, 0, -8, -4, 6, 12, 0, -12, -6, 4, 8, 0, -8, -4, 1, 2, 0, -2, -1};
	int filterSobelVertical[] = {-1, -4, -6, -4, -1, -2, -8, -12, -8, -2, 0, 0, 0, 0, 0, 2, 8, 12, 8, 2, 1, 4, 6, 4, 1 };
	int filterBlur[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	int filterN = 5;
	int divider = 21;
	int dividerBlur = 9;
	int dividerSobel = 1;

	int typeFilter = 2; //0 : sobel, 1 : filtre base, 2 : blur
	bool seuillage = false;
  bool luminance = false;

	//printf("nb pixel : %d \n", image->x*image->y);

	PPMImage *output = new PPMImage();
	output->x = image->x;
	output->y = image->y;
	output->data = new PPMPixel[output->x*output->y];

  //Si on veut effectuer un seuillage avant d'appliquer les filtres
  if(seuillage || luminance){
  for (int i = 0; i < image->x * image->y; i++) {

    int colortmp = 0.2126 * image->data[i].red + 0.7152 * image->data[i].green + 0.0722 * image->data[i].blue;

    if(seuillage){
      if(colortmp < 170)
        colortmp = 0;
      else
        colortmp = 255;
    }

    image->data[i].red = colortmp;
    image->data[i].green = colortmp;
    image->data[i].blue = colortmp;
  }
}

  //temps d'execution start
  clock_t start, end;
  float delta = 0;
  start = clock();

  int max_exe = 10000;

  for(int e = 0 ; e < max_exe ; e++)
  {
  	//Code OpenMp exemple
  	#pragma omp parallel
  	{
  		int tmpIndex;
  		//printf("num threads utilise : %d / %d\n", omp_get_num_threads(), omp_get_max_threads());

  		#pragma omp for
  		for (int i = 0; i < image->x * image->y; i++) {

  			if (typeFilter == 0) { //pour sobel

  				//On recupere la couleur du filtre horizontal et vertical
  				int *color1 = applyFilter(image, filterSobelHorizontal, filterN, dividerSobel, i);
  				int *color2 = applyFilter(image, filterSobelVertical, filterN, dividerSobel, i);

  				//On definit la couleur du pixel
  				int *color = new int[3];
  				color[0] = sqrt(pow(color1[0], 2) + pow(color2[0], 2));
  				color[1] = sqrt(pow(color1[1], 2) + pow(color2[1], 2));
  				color[2] = sqrt(pow(color1[2], 2) + pow(color2[2], 2));

				output->data[i].red = color[0];
  				output->data[i].green = color[1];
  				output->data[i].blue = color[2];

				delete color1;
				delete color2;
				delete color;

  			}
  			else{ //pour les autres filtres

  				int *color;

  				//En fonction du filtre on applique le filtre en fonction des parametres

  				if (typeFilter == 1) {
  					color = applyFilter(image, filter, filterN, divider, i);
  				}
  				else if (typeFilter == 2) { //blur
  					color = applyFilter(image, filterBlur, filterN, dividerBlur, i);
  				}

  				output->data[i].red = color[0];
  				output->data[i].green = color[1];
  				output->data[i].blue = color[2];

				delete color;
  			}

  		}
  	 }
     image = output;
   }
	//fin code OpenMp exemple
  printf("Finised \n");
  //temps execution end
  end = clock();
  delta = end - start;
  printf("Temps execution %f ms \n", delta );

	//Ecriture du fichier image
	writePPM("output.ppm", output);

	free(image);

	system("pause");
	return 0;
}
